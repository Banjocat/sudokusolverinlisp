
;TERMS to help with reading my comments
;Solved: A cell that is solved
;Possibles: Cells not solves have a list of possible answers
;Clean means a line on the grid is removed of all possibles that are solved



;DESIGN
;Input file turn into array or listd
;clean rows
;clean cols
;clean blocks
;do till each cell is solved
;Have a finite amount of times it will run before saying unsolvable
;Output to screen or file


;GRID
;Grid is list of 91 elements
;Each element is another list of possible numbers
;If the cell is solved the list is just one elememen the solution







; This is the only functon that uses a loop
; Everything else is recursive for practice
(defun print-grid (grid)
  (loop for i from 1 to 81 do
       (format t "~s" (print-cell (nth (- i 1) grid)))
       (cond ( (= (mod i 9) 0) (format t "~%")))))

(defun print-cell (cell)
  (if (>= (list-length cell) 2)
      "*"
      (string (coerce (+ 48 (car cell)) 'character))))


;Used for easy loading for manual unit testing
(defun reload () (load "sudoku.lisp"))




;Loads the sudoku
;If there is not 81 elements it alerts then stops
(defun load-sudoku (filename)

  (let ( (x (read-in (open filename))))
    (if (= (list-length x) 81)
	x
	(progn 
	  (format t "Sudoku file does not have 81 elements")
          (quit)))))


(defun solve-it (grid &optional (times 30))
  (if (<= times 1)
      (clean-blocks (clean-cols (clean-rows grid)))
      (solve-it (clean-blocks (clean-cols (clean-rows grid))) (- times 1))))
   

;Takes a stream and returns a list of the sudoku grid
;Could be used for manual entry - but I think file only is fine
(defun read-in (file)
  (if (not (listen file))
      nil
  (let ((x (read-char file)))
    (if (digit-char-p x)
     (union (list (list (parse-integer (string x)))) (read-in file))
     (if (not (whitespace-char-p x))
	 (union (list (list 1 2 3 4 5 6 7 8 9)) (read-in file))
         (read-in file))))))




;There is no check for char is a whitespace in lisp
;So I had to make one here it is
;This assumes ASCII values!
(defun whitespace-char-p (x)
  (if (< (char-code x) 33)
      t
      nil))

;Clean all the cols of a grid
;FUNCTION NEEDS TO BE TESTED
(defun clean-cols (grid &optional (start 1))
  (if (>= start 9)
      (new-col grid (clean-line (extract-col grid start)) start)
      (clean-cols (new-col grid (clean-line (extract-col grid start)) start) (1+ start))))
      

;Takes a col that was clean and places it into a new col
;This is non destructive for functional programming practice
;col is the new col to be put in
;col num is what colnum to place it in, this is not 0 indexed
(defun new-col (grid col colnum)
  (if (not (consp grid))
      nil
      ;Is it at the col?
      (if (= (mod (- 82 (list-length grid)) 9) colnum) 
	  (cons (car col) (new-col (cdr grid) (cdr col) colnum))
          ;If not then just continue on
	  (cons (car grid) (new-col (cdr grid) col colnum)))))
	  
	  

(defun extract-col (grid &optional (col 1))
  (if (<= (list-length grid) 9 )
      (cons (car grid) nil)
    ;If at beginning
    (if (= (list-length grid) 81)
	(cons (nth (- col 1) grid) (extract-col (nthcdr (+ 8 col) grid)))
        (cons (car grid) (extract-col (nthcdr 9 grid))))))

(defun extract-row (grid &optional (counter 1))

  (if (< counter 9)
      (cons (car grid) (extract-row (cdr grid) (+ 1 counter)))
      (cons (car grid) nil)))


;; This needs to be tested
(defun clean-rows (grid &optional (counter 1))
  
  (if (< counter 9)
      (union (clean-line (extract-row grid)) (clean-rows (nthcdr 9 grid) (1+ counter)))
      (clean-line (extract-row grid))))
  



;Takes a set of numbers
;Removes the number
;This deals with sets so elements should be unique
(defun remove-number (set number)
  (if (consp set)
  (if (= (car set) number)
      (cdr set) (cons (car set) (remove-number (cdr set) number)))))





;Takes a line which could be a row or col  -- Maybe block
;And goes through each to remove each number
(defun remove-number-line (line number)
  (if (consp line)
      (if (= 1 (list-length (car line)))
	  (cons 
	   (car line) 
	   (remove-number-line (cdr line) number))
	  (cons 
	   (remove-number (car line) number) 
	   (remove-number-line (cdr line) number)))))
 

;Takes a list which is to represent a col block or line
;And removes possible numbers if a single solution is already at the cell
(defun clean-line (line &optional 
			(numbers (singles-only line)))
  (if (consp numbers)
      (clean-line
        (remove-number-line line (car (car numbers)))
        (cdr numbers))
      line))


;Returns a list of numbers that are now in single cells
(defun singles-only (line)
  (if (consp line)
      (if (= 1 (list-length (car line)))
	  (cons (car line) (singles-only (cdr line)))
	  (singles-only (cdr line)))))


;Checks if the line is now complete
;Aka 9 singular cells
(defun line-done (line)
  (if (= 9 (list-length (singles-only line)))
      t nil))
  
;Gets the equation for finding the block start
(defun get-block-start (blocknum)
  (cond
    ( (= 1 blocknum) 1)
    ( (= 2 blocknum) 4)
    ( (= 3 blocknum) 7)
    ( (= 4 blocknum) 28)
    ( (= 5 blocknum) 31)
    ( (= 6 blocknum) 34)
    ( (= 7 blocknum) 55)
    ( (= 8 blocknum) 58)
    ( (= 9 blocknum) 61)))


(defun list-of-blocks (start)
  (list start (+ start 1) (+ start 2)
        (+ start 9) (+ start 10) (+ start 11)
        (+ start 18) (+ start 19) (+ start 20)))

(defun is-in-block? (i blocknum)

  (let ((start (get-block-start blocknum)))
    (if (member i (list-of-blocks start))
        t
        nil)))
    


(defun clean-blocks (grid &optional (start 1))
  (if (>= start 9)
      (new-block grid (clean-line (get-block grid start)) start)
      (clean-blocks (new-block grid (clean-line (get-block grid start)) start)
                    (+ start 1))))

(defun get-block (grid blocknum)
  (if (not (consp grid))
      nil
  (if (is-in-block? (- 82 (list-length grid)) blocknum)
      (cons (car grid) (get-block (cdr grid) blocknum))
      (get-block (cdr grid) blocknum))))


(defun new-block (grid block blocknum)
  (if (not (consp grid))
      nil
  (if (is-in-block? (- 82 (list-length grid)) blocknum)
      (cons (car block) (new-block (cdr grid) (cdr block) blocknum))
      (cons (car grid)  (new-block (cdr grid) block blocknum)))))




(print-grid (solve-it (load-sudoku (car *args*))))
